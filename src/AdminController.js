
const api_url = 'http://localhost:8080/api/robots';
const api_batalla = 'http://localhost:8080/api/batalla';

export default class Controller {

    //Devuelve todos los robots
    static getAll = () => {
        return new Promise(
            (res, rej) => {
                fetch(api_url)
                    .then(data => data.json())
                    .then(datos => {
                        const retorn = datos.map(el => {
                            el.id = el._id;
                            return el;
                        });
                        res(retorn);
                    })
                    .catch(err => {
                        rej(err);
                    });
            });

    }

    //Muestra detalle de todos los robots
    static getById = (itemId) => {
        const promesa = (res, rej) => {
            fetch(api_url + '/' + itemId)
                .then(data => data.json())
                .then(robot => {
                    robot.id = robot._id;
                    res(robot);
                })
                .catch(err => {
                    rej(err);
                });
        };
        return new Promise(promesa);
    }

    //Añade un robot --- OK
    static addRobot = (item) => {
        const jsonContacte = JSON.stringify(item);
        const opcionesFetch = {
            method: "POST",
            body: jsonContacte,
            headers: { 'Content-Type': 'application/json' },
        }

        fetch(api_url, opcionesFetch)
            .then(resp => {
                console.log("nuevo contacto:", resp)
            })
            .catch(err => console.log("error nuevo contacto", err));
    }

    static combate = (item1,item2) => {  
        const promesa = (resuelve, falla) => {
            fetch(api_batalla+"/"+item1._id+"/"+item2._id)
                .then(data => data.json())
                .then(ganador => {
                    resuelve(ganador);
                })
                .catch(err => {
                    falla(err);
                });
        };

        return new Promise(promesa);
    }

    static setRobot = (item) => {
        const jsonRobot = JSON.stringify(item);
        const opcionesFetch = {
            method: "PUT",
            body: jsonRobot,
            headers: { 'Content-Type': 'application/json' },
        }

        fetch(api_url+"/"+item._id, opcionesFetch)
            .then(resp => {
                console.log("robot modificado", resp)
            })
            .catch(err => console.log("error robot modificado", err));
    }
}


