import React, { useCallback, useEffect, useState } from 'react';
import { Container, Row, Col, Button, Card, CardBody, CardImg, CardTitle, CardSubtitle, Collapse } from 'reactstrap';
import { Link } from "react-router-dom";

import Controller from "./AdminController";

const Play = () => {

    const [mensaje, setMensaje] = useState("ELIGE LOS ROBOTS");
    const [robot1, setRobot1] = useState(false);
    const [robot2, setRobot2] = useState(false);
    const [robotx, setRobotx] = useState(false);
    const [ganador, setGanador] = useState(false);

    useEffect(() => {
        if (!robot1.nombre) {
            setRobot1(robotx);
        } else {
            setRobot2(robotx);
            setMensaje("DALE A FIGHT!");
        }
    }, [robotx]);

    useEffect(() => {
        if (!ganador) {
            return
        }
        setMensaje("GANA " + ganador.nombre + "!");
    }, [ganador]);

    const partida = (robot1, robot2) => {

        const tempRobot1 = { ...robot1 };
        const tempRobot2 = { ...robot2 };


        Controller.combate(robot1, robot2)
            .then(data => setGanador(data))
            .then(() => {
                if (ganador === robot1) {
                    tempRobot1.experiencia.victorias++;
                    tempRobot1.experiencia.jugadas++;
                    tempRobot2.experiencia.jugadas++;
                } else {
                    tempRobot2.experiencia.victorias++;
                    tempRobot2.experiencia.jugadas++;
                    tempRobot1.experiencia.jugadas++;
                }
            })
            .then(() => Controller.setRobot(tempRobot1))
            .then(() => Controller.setRobot(tempRobot2))
            .then(() => setRobot1(false))
            .then(() => setRobot2(false))
            .catch(err => console.log(err.message));
    }

    const sumaEstadisticas = (robot) => {
        console.log("stats:", robot)
        Controller.setRobot(robot)
            .then(data => console.log(data))
            .catch(err => console.log(err.message))

    }

    const [robots, setRobots] = useState([]);
    useEffect(() => {
        Controller.getAll()
            .then(datos => {

                return (

                    datos.map((el) => {

                        return (
                            <div className="robot-card" key={el._id} onClick={() => setRobotx(el)}>
                                <img src={"https://robohash.org/" + el.nombre + "?set=set3"} alt={el.nombre} width="60px" />
                                <h4>{el.nombre}</h4>
                            </div>

                        )
                    }));
            })
            .then(data => setRobots(data))
            .catch(err => console.log(err.message));
    }, [])




    return (
        <>
                <Link className="btn btn-secondary btn-sm p-3 m-4 float-left" to="/">
                    <i className="fa fa-home" aria-hidden="true"></i>
                </Link>
                <div className="message">
                    <h2>{mensaje}</h2>
                </div>
            <Container>
                <Row>
                    <Col>
                        <div className="deck">
                            {robots}
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col>
                    <Collapse isOpen={robot1}>
                    <Card className="selected-card" >
                            <CardImg top width="100%" src={"https://robohash.org/" + robot1.nombre + "?set=set3"} alt={robot1.nombre} />
                            <CardBody>
                                <CardTitle tag="h4">{robot1.nombre}</CardTitle>
                            </CardBody>
                        </Card>
                    </Collapse>
                    </Col>
                    <Col className="d-flex flex-column justify-content-center">
                        <h1>VS</h1>
                        <Button className='btn btn-secondary p-4 mt-4' onClick={() => partida(robot1, robot2)}>FIGHT</Button>
                    </Col>
                    <Col>
                    <Collapse isOpen={robot2}>
                    <Card className="selected-card" >
                            <CardImg top width="100%" src={"https://robohash.org/" + robot2.nombre + "?set=set3"} alt={robot2.nombre} />
                            <CardBody>
                                <CardTitle tag="h4">{robot2.nombre}</CardTitle>
                            </CardBody>
                        </Card>
                    </Collapse>
                    </Col>
                </Row>
            </Container>

        </>
    )
}

export default Play;