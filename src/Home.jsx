import { Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button } from 'reactstrap';


function Home() {

    const marco = {
        'width': '30px',
        'height': '300px',
        'display': 'inline-block',
        'position': 'absolute',
        'top': '40%',
        'left': '42%'
    }

    const titulo = {
        'marginTop': '40px',
        'color': '#D3CEDF',
        'fontSize': '325%'
    }

    const boton = {
        'width': '200px',
        'height': '60px',
        'backgroundColor': '#9991AA',
        'border': '1px solid #45813D',
        'margin': '5px',
        'color': 'black',
        'fontFamily': 'Goldman',
    }
    return (
        <>
            <div>
                <h1 style={titulo}>BATALLA DE ROBOTS</h1>
            </div>
            <div style={marco}>
                <Link to="/play" className='hov'>
                    <Button style={boton} >
                        <span>Play</span>
                    </Button>
                </Link>
                <Link to="/admin" className='hov'>
                    <Button style={boton}>
                        <span>Administración</span>
                    </Button>
                </Link>
            </div>
        </>

    );
}

export default Home; 
