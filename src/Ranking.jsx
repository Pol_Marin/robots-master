import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Button, Container, Table } from 'reactstrap';

import Controller from './AdminController';

const Ranking = (props) => {

    const [dades, setDades] = useState([]);
    const [error, setError] = useState('');

    useEffect(() => {

        Controller.getAll()

            .then(robots => {

                return (

                    robots.sort((a, b) => (a.experiencia.victorias > b.experiencia.victorias) ? -1 : 1)

                        .map((el, idx) => {

                            el.ranking = idx + 1;

                            return (

                                <tr key={el._id}>
                                    <td>{el.ranking}</td>
                                    <td><img src={"https://robohash.org/" + el.nombre + "?set=set3"} alt={el.nombre} width="60px" /></td>
                                    <td>{el.nombre}</td>
                                    <td>{el.caracteristicas.ataque}</td>
                                    <td>{el.caracteristicas.defensa}</td>
                                    <td>{el.caracteristicas.vida}</td>
                                    <td>{el.experiencia.victorias}</td>
                                    <td>{el.experiencia.jugadas}</td>
                                    <td>
                                        <Link to={"/admin/" + el._id +"/"+el.ranking}>
                                            <Button ><i className="fa fa-eye" aria-hidden="true"></i></Button>
                                        </Link>
                                    </td>
                                </tr>
                            )
                        }));
            })
            .then(data => setDades(data))
            .catch(err => setError(err.message));
    }, [])

    if (error) console.log(error)


    return (
        <>
                <Container className="pb-5">
                    <Table dark >
                        <thead>
                            <tr>
                                <th>Ranking</th>
                                <th></th>
                                <th>Nombre</th>
                                <th>Ataque</th>
                                <th>Defensa</th>
                                <th>Vida</th>
                                <th>Victorias</th>
                                <th>Jugadas</th>
                                <th>Detalle</th>
                            </tr>
                        </thead>
                        <tbody>
                            {dades}
                        </tbody>
                    </Table>
                </Container>
        </>
    )
};

export default Ranking;