import React, { useState } from "react";
import { Container, Row, Col, FormGroup, Label, Input, Button, InputGroup, InputGroupAddon, Alert, Collapse } from 'reactstrap';
import { Redirect, Link } from "react-router-dom";

import Controller from './AdminController';

const Nuevo = (props) => {

    const [nom, setNom] = useState('');
    const [ataque, setAtaque] = useState(1);
    const [defensa, setDefensa] = useState(1);
    const [vida, setVida] = useState(1);
    const [maxAtributos, setMaxAtributos] = useState(10);

    const [volver, setVolver] = useState();
    const [alert, setAlert] = useState(false);

    const guardar = () => {
        const robotNuevo = {
            nombre: nom,
            ataque,
            defensa,
            vida,
        };

        if (robotNuevo.nombre !== "" && maxAtributos === 0) {
            Controller.addRobot(robotNuevo);
            setAlert(false);
            setVolver(true);
        } else {
            setAlert(true);
        }
    }


    if (volver) {
        return <Redirect to="/admin" />
    }


    return (
        <>
            <Container>
                <h3 className="mt-4">CREA UN NUEVO ROBOT</h3>
                
                <FormGroup>
                    <Label for="nom">NOMBRE</Label>
                    <Input type="text" name="nom" id="nom" value={nom} onChange={(e) => setNom(e.target.value)} />
                </FormGroup>

                <Collapse isOpen={alert}>
                    <Alert color="danger">
                        Ponle nombre a tu robot y gasta todos sus puntos para guardar!
                    </Alert>
                </Collapse>

                <h4 className="mt-4">Total: {maxAtributos}</h4>                

                <Label for="ataque">ATAQUE</Label>
                <InputGroup>
                    <InputGroupAddon addonType="prepend">
                        <Button onClick={() => {
                            if (ataque > 1) {
                                setMaxAtributos(maxAtributos + 1)
                                setAtaque(ataque - 1)
                            }
                        }}>-</Button>
                    </InputGroupAddon>
                    <Input value={ataque} readOnly />
                    <InputGroupAddon addonType="append">
                        <Button onClick={() => {
                            if (maxAtributos > 0) {
                                setMaxAtributos(maxAtributos - 1)
                                setAtaque(ataque + 1)
                            }
                        }} >+</Button>
                    </InputGroupAddon>
                </InputGroup>
                <br />

                <Label for="defensa">DEFENSA</Label>
                <InputGroup>
                    <InputGroupAddon addonType="prepend">
                        <Button onClick={() => {
                            if (defensa > 1) {
                                setMaxAtributos(maxAtributos + 1)
                                setDefensa(defensa - 1)
                            }
                        }}>-</Button>
                    </InputGroupAddon>
                    <Input value={defensa} readOnly />
                    <InputGroupAddon addonType="append">
                        <Button onClick={() => {
                            if (maxAtributos > 0) {
                                setMaxAtributos(maxAtributos - 1)
                                setDefensa(defensa + 1)
                            }
                        }} >+</Button>
                    </InputGroupAddon>
                </InputGroup>
                <br />

                <Label for="vida">VIDA</Label>
                <InputGroup>
                    <InputGroupAddon addonType="prepend">
                        <Button onClick={() => {
                            if (vida > 1) {
                                setMaxAtributos(maxAtributos + 1)
                                setVida(vida - 1)
                            }
                        }}>-</Button>
                    </InputGroupAddon>
                    <Input value={vida} readOnly />
                    <InputGroupAddon addonType="append">
                        <Button onClick={() => {
                            if (maxAtributos > 0) {
                                setMaxAtributos(maxAtributos - 1)
                                setVida(vida + 1)
                            }
                        }} >+</Button>
                    </InputGroupAddon>
                </InputGroup>

                <br />
                <Row>
                    <Col>
                    <Link className='btn btn-secondary p-4 mt-4' to='/admin' >VOLVER</Link>
                    </Col>
                    <Col>
                <Button className='btn btn-secondary p-4 mt-4' onClick={guardar} >GUARDAR</Button>

                    </Col>
                </Row>
            </Container>
        </>
    );
};


export default Nuevo;