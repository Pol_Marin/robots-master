import { BrowserRouter, Switch, Route } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import Play from './Play';
import Admin from './Admin';
import Nuevo from './Nuevo';
import Detalle from './Detalle';
import Home from './Home';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/play" component={Play} />
        <Route exact path="/admin" component={Admin} />
        <Route exact path="/admin/nuevo" component={Nuevo} />
        <Route path="/admin/:id/:ranking" component={Detalle} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;