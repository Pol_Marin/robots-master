import React from "react";

import { Link } from "react-router-dom";
import { Button, Container } from "reactstrap";

import Ranking from './Ranking';

const Admin = () => {

    return (
        <>
            <div className="start"></div>
            <Link className="btn btn-secondary btn-sm p-3 m-4 float-left" to="/">
                    <i className="fa fa-home" aria-hidden="true"></i>
                </Link>
            <Container className="contenedor">
                <Link className="btn btn-secondary btn-sm p-3 m-4" to="/admin/nuevo">
                    CREAR NUEVO ROBOT
                </Link>
                <br />
                <br />
                <Ranking />
            </Container>

        </>
    )
};

export default Admin;