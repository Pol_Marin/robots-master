import React, { useState, useEffect } from "react";
import { Redirect, Link } from "react-router-dom";

import { Container, Row, Col, Table } from 'reactstrap';

import Controller from './AdminController';

const Detalle = (props) => {

    const [robot, setRobot] = useState({});
    const [caract, setCaract] = useState({});
    const [exp, setExp] = useState({});

    const id = props.match.params.id;
    const ranking = props.match.params.ranking;

    useEffect(() => {
        Controller.getById(id)
            .then(data => {
                setRobot(data)
                setCaract(data.caracteristicas)
                setExp(data.experiencia)
            })
            .catch(err => {
                return <Redirect to="/admin" />;
            });
    }, [id])


    return (
        <>
            <Container className="pt-5 d-flex flex-wrap flex-column align-items-center">
                <h1 >{robot.nombre}</h1>
                <Row>
                    <Col xs="12" lg="8">
                        <Table dark className="mt-4">
                            <thead>
                                <tr>
                                    <th>ATAQUE</th>
                                    <th>DEFENSA</th>
                                    <th>VIDA</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{caract.ataque}</td>
                                    <td>{caract.defensa}</td>
                                    <td>{caract.vida}</td>
                                </tr>
                            </tbody>
                        </Table>
                        <Table dark className="mt-4">
                            <thead>
                                <tr>
                                    <th>RANKING</th>
                                    <th>PARTIDAS</th>
                                    <th>GANADAS</th>
                                    <th>PERDIDAS</th>
                                    <th>RATTING</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{ranking}</td>
                                    <td>{exp.jugadas}</td>
                                    <td>{exp.victorias}</td>
                                    <td>{exp.jugadas - exp.victorias}</td>
                                    <td>{Math.round((exp.victorias / exp.jugadas) * 100)}%</td>
                                </tr>
                            </tbody>
                        </Table>
                    </Col>
                    <Col xs="12" lg="4">
                        <img src={"https://robohash.org/" + robot.nombre + "?set=set3"} alt={robot.nombre} />
                    </Col>
                </Row>
                <Link className='btn btn-secondary p-4 mt-4' to='/admin' >VOLVER AL LISTADO DE ROBOTS</Link>
            </Container>
        </>
    );
};

export default Detalle;